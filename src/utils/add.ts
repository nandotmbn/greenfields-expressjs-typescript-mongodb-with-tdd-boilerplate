function add(numbers: number[]) {
	let results = 0;
	numbers.forEach((number) => {
		results += number;
	});

	return results;
}

export { add };
