import job from "node-cron";

function cronJob() {
	job.schedule("* * * * *", () => {
		console.log("Running a task every minute");
	});

	console.log("[cron]: Cron Job Successfully Started!");

	return true;
}

export { cronJob };
