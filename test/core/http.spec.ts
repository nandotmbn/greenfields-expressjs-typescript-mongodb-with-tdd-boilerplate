import { app, http, port } from "../../src/core/http.core";

describe("Core Test: HTTP", () => {
	describe("Express Modules", () => {
		test(`It should be not null`, () => {
			expect(app).not.toBeNull();
		});
		test(`It should be not falsy`, () => {
			expect(app).not.toBeFalsy();
		});
	});

	describe("PORT", () => {
		test(`It should be not null`, () => {
			expect(port).not.toBeNull();
		});
		test(`It should be not falsy`, () => {
			expect(port).not.toBeFalsy();
		});
		test(`It should be greated than 0`, () => {
			expect(port).toBeGreaterThan(0);
		});
	});

	describe("HTTP", () => {
		test(`It should be not null`, () => {
			expect(http).not.toBeNull();
		});
		test(`It should be not falsy`, () => {
			expect(http).not.toBeFalsy();
		});
	});
});
